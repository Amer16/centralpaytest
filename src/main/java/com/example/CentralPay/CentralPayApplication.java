package com.example.CentralPay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CentralPayApplication {

	public static void main(String[] args) {
		SpringApplication.run(CentralPayApplication.class, args);
	}

}
