package com.example.CentralPay.controller;
import com.example.CentralPay.models.Url;
import com.example.CentralPay.services.URLService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/url")
public class URLController {
    @Autowired
    private URLService urlService;

    // Endpoint pour raccourcir une URL
   @PostMapping("/shorten")

   public ResponseEntity<?> shortenUrl(@RequestBody Map<String, String> requestBody) {
       String originalURL = requestBody.get("originalURL");
       if (originalURL != null && (originalURL.startsWith("http://") || originalURL.startsWith("https://"))) {
           String shortenedUrl = urlService.shortenURL(originalURL);
           return ResponseEntity.ok(shortenedUrl);
       } else {
           // Créez une instance de ErrorResponse avec le message approprié
           ErrorResponse errorResponse = new ErrorResponse("L'URL doit être fournie et commencer par https:// ou http://");
           // Retournez une réponse avec le code d'erreur 400 et le corps JSON
           return ResponseEntity.badRequest().body(errorResponse);
       }
   }

    // Endpoint pour rediriger vers l'URL d'origine à partir du code raccourci
    @GetMapping("/original/{shortCode}")
    public ResponseEntity<?> redirectToOriginalURL(@PathVariable String shortCode) {
        String originalURL = urlService.getOriginalURL(shortCode);
        if (originalURL != null) {
            return ResponseEntity.ok(originalURL);
        } else {
            // Gérer le cas où le code raccourci n'existe pas
            ErrorResponse errorResponse = new ErrorResponse("Le code raccourci  n'existe pas.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(errorResponse);
        }
    }

    // Endpoint pour obtenir toutes les URLs
    @GetMapping("/all")
    public List<Url> getAllURLs() {
        return urlService.getAllURLs();
    }

    // Endpoint pour obtenir le nombre de visites pour une URL raccourcie
    @GetMapping("/{shortCode}/visits")
    public ResponseEntity<?> getVisitCount(@PathVariable String shortCode) {
        try {
            int visitCount = urlService.getVisitCount(shortCode);
            return ResponseEntity.ok(visitCount);
        } catch (ChangeSetPersister.NotFoundException ex) {
            ErrorResponse errorResponse = new ErrorResponse("URL not found");
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(errorResponse);
        }
    }
    // Endpoint pour vérifier si une URL raccourcie existe
    @GetMapping("/exists/{shortCode}")
    public ResponseEntity<Boolean> checkShortURLExists(@PathVariable String shortCode) {
        boolean exists = urlService.checkShortURLExists(shortCode);
        return ResponseEntity.ok(exists);
    }
    // Endpoint pour obtenir toutes les URLs raccourcies
    @GetMapping("/shortUrls")
    public List<String> getAllShortURLs() {
        return urlService.getAllShortURLs();
    }

}
