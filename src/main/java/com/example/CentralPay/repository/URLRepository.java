package com.example.CentralPay.repository;
import com.example.CentralPay.models.Url;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface URLRepository extends MongoRepository<Url, String> {
    Url findByShortCode(String shortCode);
    Url findByOriginalURL(String originalURL);
    boolean existsByShortCode(String shortCode);
//    List<String> findAllShortCodes();
    List<Url> findAll();


}
