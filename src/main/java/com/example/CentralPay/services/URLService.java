package com.example.CentralPay.services;
import com.example.CentralPay.models.Url;
import com.example.CentralPay.repository.URLRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class URLService {


    @Autowired
    private URLRepository URLRepository;

    // Méthode pour raccourcir une URL
    public String shortenURL(String originalURL) {

        Url existingURL = URLRepository.findByOriginalURL(originalURL);

        if (existingURL != null) {
            return existingURL.getShortCode();
        } else {
            // Générer un nouveau code court pour l'URL
            String shortCode = generateShortCode();

            // Créer un nouvel objet Url avec l'URL d'origine et le code court généré
            Url newShortURL = new Url();
            newShortURL.setOriginalURL(originalURL);
            newShortURL.setShortCode(shortCode);

            URLRepository.save(newShortURL);

            return shortCode;
        }
    }

    // Méthode pour générer un code court aléatoire
    public String generateShortCode() {
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 6; i++) {
            sb.append(chars.charAt(random.nextInt(chars.length())));
        }
        return sb.toString();
    }

    // Méthode pour vérifier si un code court existe déjà dans la base de données
    public boolean checkShortURLExists(String shortCode) {
        return URLRepository.existsByShortCode(shortCode);
    }

    // Méthode pour récupérer tous les codes courts existants dans la base de données
    public List<String> getAllShortURLs() {
        List<Url> urls = URLRepository.findAll();
        return urls.stream()
                .map(Url::getShortCode)
                .collect(Collectors.toList());
    }

    // Méthode pour récupérer l'URL d'origine à partir du code court
    public String getOriginalURL(String shortCode) {
        Url shortURL = URLRepository.findByShortCode(shortCode);
        if (shortURL != null) {
            // Incrémenter le compteur de visites et sauvegarder dans la base de données
            shortURL.setVisitCount(shortURL.getVisitCount() + 1);
            URLRepository.save(shortURL);
            return shortURL.getOriginalURL();
        } else {
            return null;
        }
    }

    // Méthode pour récupérer le nombre de visites pour un code court donné
    public int getVisitCount(String shortCode) throws ChangeSetPersister.NotFoundException {
        Url url = URLRepository.findByShortCode(shortCode);
        if (url != null) {
            return url.getVisitCount();
        } else {
            throw new ChangeSetPersister.NotFoundException();
        }
    }

    // Méthode pour récupérer toutes les URL stockées dans la base de données
    public List<Url> getAllURLs() {
        return URLRepository.findAll();
    }
}