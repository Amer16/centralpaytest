package com.example.CentralPay.models;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Data
public class Url {
    @Id
    private String id;
    private String originalURL;
    private String shortCode;
    private int visitCount; // pour le compteur de visites

}
