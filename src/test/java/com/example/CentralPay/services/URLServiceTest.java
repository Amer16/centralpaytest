package com.example.CentralPay.services;

import com.example.CentralPay.models.Url;
import com.example.CentralPay.repository.URLRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.crossstore.ChangeSetPersister;

import java.util.Arrays;
import java.util.List;

import static com.mongodb.assertions.Assertions.assertFalse;
import static com.mongodb.assertions.Assertions.assertNull;
import static com.mongodb.internal.connection.tlschannel.util.Util.assertTrue;
import static org.bson.assertions.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

 class URLServiceTest {


    @Mock
    private URLRepository URLRepository;

    @InjectMocks
    private URLService urlService;

     @BeforeEach
     public void setUp() {
         MockitoAnnotations.openMocks(this);
     }

     // Test de la méthode shortenURL pour une URL existante
     @Test
     void testShortenURL_ExistingURL() {
         // Given
         String originalURL = "https://www.example.com";
         Url existingURL = new Url();
         existingURL.setOriginalURL(originalURL);
         existingURL.setShortCode("abc123");

         //  comportement attendu du mock URLRepository
         when(URLRepository.findByOriginalURL(originalURL)).thenReturn(existingURL);

         // When
         String shortCode = urlService.shortenURL(originalURL);

         // Then
         assertEquals("abc123", shortCode);
         verify(URLRepository, times(1)).findByOriginalURL(originalURL);
         verify(URLRepository, never()).save(any(Url.class));
     }

     // Test de la méthode shortenURL pour une nouvelle URL
     @Test
     void testShortenURL_NewURL() {
         // Given
         String originalURL = "https://www.example.com";

         // comportement attendu du mock URLRepository
         when(URLRepository.findByOriginalURL(originalURL)).thenReturn(null);

         // When
         String shortCode = urlService.shortenURL(originalURL);

         // Then
         verify(URLRepository, times(1)).findByOriginalURL(originalURL);
         verify(URLRepository, times(1)).save(any(Url.class));

         assertNotNull(shortCode);
     }

     // Test de la méthode generateShortCode
     @Test
     void testGenerateShortCode() {
         // Given
         URLService urlService = new URLService();

         // When
         String shortCode = urlService.generateShortCode();

         // Then
         assertNotNull(shortCode);
         assertEquals(6, shortCode.length());
         assertTrue(shortCode.matches("[a-zA-Z0-9]{6}"));
     }

     // Test de la méthode checkShortURLExists pour un code existant
     @Test
     void testCheckShortURLExists_Exists() {
         // Given
         String shortCode = "abc123";
         when(URLRepository.existsByShortCode(shortCode)).thenReturn(true);

         // When
         boolean exists = urlService.checkShortURLExists(shortCode);

         // Then
         assertTrue(exists);
         verify(URLRepository, times(1)).existsByShortCode(shortCode);
     }

     // Test de la méthode checkShortURLExists pour un code inexistant
     @Test
     void testCheckShortURLExists_NotExists() {
         // Given
         String shortCode = "def456";
         when(URLRepository.existsByShortCode(shortCode)).thenReturn(false);

         // When
         boolean exists = urlService.checkShortURLExists(shortCode);

         // Then
         assertFalse(exists);
         verify(URLRepository, times(1)).existsByShortCode(shortCode);
     }

     // Test de la méthode getAllShortURLs
     @Test
     void testGetAllShortURLs() {
         // Given
         List<Url> urls = Arrays.asList(
                 createUrl("abc123"),
                 createUrl("def456"),
                 createUrl("ghi789")
         );

         when(URLRepository.findAll()).thenReturn(urls);

         // When
         List<String> shortURLs = urlService.getAllShortURLs();

         // Then
         assertEquals(3, shortURLs.size());
         assertTrue(shortURLs.contains("abc123"));
         assertTrue(shortURLs.contains("def456"));
         assertTrue(shortURLs.contains("ghi789"));
         verify(URLRepository, times(1)).findAll();
     }


     private Url createUrl(String shortCode) {
        Url url = new Url();
        url.setShortCode(shortCode);
        return url;
    }

     // Test de la méthode getOriginalURL pour un code existant
     @Test
     void testGetOriginalURL_Exists() {
         // Given
         String shortCode = "abc123";
         Url shortURL = new Url();
         shortURL.setOriginalURL("https://www.example.com");
         shortURL.setVisitCount(0);

         when(URLRepository.findByShortCode(shortCode)).thenReturn(shortURL);

         // When
         String originalURL = urlService.getOriginalURL(shortCode);

         // Then
         assertEquals("https://www.example.com", originalURL);
         assertEquals(1, shortURL.getVisitCount());
         verify(URLRepository, times(1)).findByShortCode(shortCode);
         verify(URLRepository, times(1)).save(shortURL);
     }


     // Test de la méthode getOriginalURL pour un code inexistant
     @Test
     void testGetOriginalURL_NotExists() {
         // Given
         String shortCode = "def456";

         when(URLRepository.findByShortCode(shortCode)).thenReturn(null);

         // When
         String originalURL = urlService.getOriginalURL(shortCode);

         // Then
         assertNull(originalURL);
         verify(URLRepository, times(1)).findByShortCode(shortCode);
         verify(URLRepository, never()).save(any(Url.class));
     }

     // Test de la méthode getVisitCount pour un code existant
     @Test
     void testGetVisitCount_Exists() throws ChangeSetPersister.NotFoundException {
         // Given
         String shortCode = "abc123";
         Url url = new Url();
         url.setVisitCount(5);

         // Définition du comportement attendu du mock URLRepository
         when(URLRepository.findByShortCode(shortCode)).thenReturn(url);

         // When
         int visitCount = urlService.getVisitCount(shortCode);

         // Then
         assertEquals(5, visitCount);
         verify(URLRepository, times(1)).findByShortCode(shortCode);
     }


     // Test de la méthode getVisitCount pour un code inexistant
     @Test
     void testGetVisitCount_NotExists() {
         // Given
         String shortCode = "def456";

         when(URLRepository.findByShortCode(shortCode)).thenReturn(null);

         // Then
         assertThrows(ChangeSetPersister.NotFoundException.class, () -> {
             // When
             urlService.getVisitCount(shortCode);
         });
         verify(URLRepository, times(1)).findByShortCode(shortCode);
     }

     // Test de la méthode getAllURLs
     @Test
     void testGetAllURLs() {
         // Given
         List<Url> urls = Arrays.asList(
                 createUrl("abc123", "https://www.example.com"),
                 createUrl("def456", "https://www.example.org"),
                 createUrl("ghi789", "https://www.example.net")
         );

         when(URLRepository.findAll()).thenReturn(urls);

         // When
         List<Url> allURLs = urlService.getAllURLs();

         // Then
         assertEquals(3, allURLs.size());
         assertEquals("https://www.example.com", allURLs.get(0).getOriginalURL());
         assertEquals("https://www.example.org", allURLs.get(1).getOriginalURL());
         assertEquals("https://www.example.net", allURLs.get(2).getOriginalURL());
         verify(URLRepository, times(1)).findAll();
     }

     private Url createUrl(String shortCode, String originalURL) {
        Url url = new Url();
        url.setShortCode(shortCode);
        url.setOriginalURL(originalURL);
        return url;
    }
}
