package com.example.CentralPay.controller;

import com.example.CentralPay.services.URLService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.bson.assertions.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

 class URLControllerTest {

     // Mock du service URL
     @Mock
     private URLService urlService;

     // Injection des mocks dans le contrôleur URL
     @InjectMocks
     private URLController urlController;

     // Initialisation des mocks avant chaque test
     @BeforeEach
     public void setUp() {
         MockitoAnnotations.initMocks(this);
     }

     // Test de la méthode shortenUrl avec une URL valide
    @Test
    void testShortenUrlWithValidURL() {
        // Given
        String originalURL = "https://www.example.com";
        String shortenedURL = "http://short.url/abc123";
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("originalURL", originalURL);

        // comportement attendu du service mocké
        when(urlService.shortenURL(originalURL)).thenReturn(shortenedURL);

        // When
        ResponseEntity<?> responseEntity = urlController.shortenUrl(requestBody);

        // Then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.getBody() instanceof String);
        assertEquals(shortenedURL, responseEntity.getBody());
    }

     // Test de la méthode shortenUrl avec une URL invalide
    @Test
    void testShortenUrlWithInvalidURL() {
        // Given
        String originalURL = "invalidURL";
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("originalURL", originalURL);

        // When
        ResponseEntity<?> responseEntity = urlController.shortenUrl(requestBody);

        // Then
        assertEquals(HttpStatus.BAD_REQUEST, responseEntity.getStatusCode());
        assertTrue(responseEntity.getBody() instanceof ErrorResponse);
        ErrorResponse errorResponse = (ErrorResponse) responseEntity.getBody();
        assertEquals("L'URL doit être fournie et commencer par https:// ou http://", errorResponse.getMessage());
    }

     // Test de la méthode redirectToOriginalURL avec un code court existant
     @Test
     void testRedirectToOriginalURLWithExistingShortCode() {
         // Given
         String shortCode = "abc123";
        String originalURL = "https://example.com";

        when(urlService.getOriginalURL(shortCode)).thenReturn(originalURL);

         // When
        ResponseEntity<?> responseEntity = urlController.redirectToOriginalURL(shortCode);

         // Then
        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(originalURL, responseEntity.getBody());
    }

     // Test de la méthode getVisitCount
    @Test
    void testGetVisitCount() throws ChangeSetPersister.NotFoundException {
        // Given
        String shortCode = "abc123";
        int expectedVisitCount = 5;

        when(urlService.getVisitCount(shortCode)).thenReturn(expectedVisitCount);

        // When
        ResponseEntity<?> responseEntity = urlController.getVisitCount(shortCode);

        // Then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.getBody() instanceof Integer);
        assertEquals(expectedVisitCount, (int) responseEntity.getBody());
    }

     // Test de la méthode checkShortURLExists lorsque le code court existe
    @Test
    void testCheckShortURLExistsWhenExists() {
        // Given
        String shortCode = "abc123";

        when(urlService.checkShortURLExists(shortCode)).thenReturn(true);

        // When
        ResponseEntity<Boolean> responseEntity = urlController.checkShortURLExists(shortCode);

        // Then
        assertEquals(true, responseEntity.getBody());
    }

     // Test de la méthode checkShortURLExists lorsque le code court n'existe pas
    @Test
    void testCheckShortURLExistsWhenDoesNotExist() {
        // Given
        String shortCode = "nonExistingCode";

        when(urlService.checkShortURLExists(shortCode)).thenReturn(false);

        // When
        ResponseEntity<Boolean> responseEntity = urlController.checkShortURLExists(shortCode);

        // Then
        assertEquals(false, responseEntity.getBody());
    }

     // Test de la méthode getAllShortURLs
     @Test
    void testGetAllShortURLs() {
         // Given
         List<String> expectedShortURLs = Arrays.asList("shortURL1", "shortURL2");

        when(urlService.getAllShortURLs()).thenReturn(expectedShortURLs);

         // When
         List<String> actualShortURLs = urlController.getAllShortURLs();

         // Then
        assertEquals(expectedShortURLs.size(), actualShortURLs.size());
        assertEquals(expectedShortURLs, actualShortURLs);
    }
}